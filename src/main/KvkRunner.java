package main;

import main.clients.RESTClient;
import main.clients.StompListener;
import main.clients.StompSender;
import main.stomp.ConnectionFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/*
 * Runs the KvkModule
 */
public final class KvkRunner
{
    // Properties.
    private static final Properties CONFIG = new Properties();

    // We don't want this class to be instantiated.
    private KvkRunner()
    {

    }

    /**
     * @param args Program arguments.
     */
    public static void main(String[] args) throws IOException
    {
        // Read the configuration.
        readConfig();
        // Initialize the connection factory.
        final ConnectionFactory connectionFactory = getConnectionFactory();
        // Create the STOMP listener.
        StompListener listener = new StompListener(connectionFactory, CONFIG.getProperty("requestQueue"));
        // And the STOMP sender.
        StompSender sender = new StompSender(connectionFactory, CONFIG.getProperty("responseQueue"));
        // REST client.
        RESTClient restClient = new RESTClient();
        // Start the KvkCore service.
        new KvkCore(listener, sender, restClient, CONFIG.getProperty("serviceUrl")).start();
    }

    /**
     * Read the CONFIG file for application settings
     */
    private static void readConfig() throws IOException
    {
        String fn = "/main/config/config.properties";
        try (InputStream is = KvkCore.class.getResourceAsStream(fn))
        {
            // Open CONFIG.properties
            if (is == null)
                throw new FileNotFoundException("File " + fn + " not found in class path");
            CONFIG.load(is);
        }
    }

    /**
     * Initialize the STOMP connection factory.
     * @return The STOMP connection factory.
     */
    private static ConnectionFactory getConnectionFactory()
    {
        String brokerHostname = CONFIG.getProperty("brokerUrl");
        int brokerPort = Integer.valueOf(CONFIG.getProperty("brokerPort"));
        String username = CONFIG.getProperty("user");
        String password = CONFIG.getProperty("pass");
        String virtualHost = CONFIG.getProperty("brokerVirtualHost");
        final ConnectionFactory connectionFactory;
        if (virtualHost == null || virtualHost.length() == 0)
            // If no virtual host is configured, attempt the default.
            connectionFactory = new ConnectionFactory(brokerHostname, brokerPort, username, password);
        else
            connectionFactory = new ConnectionFactory(brokerHostname, brokerPort, username, password, virtualHost);
        return connectionFactory;
    }
}
