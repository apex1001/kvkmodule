package main.entity;

/**
 * Search request class.
 */
public class SearchRequest
{
    // The id related to the search request.
    private String id;
    // The search request itself.
    private Search request;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Search getRequest()
    {
        return request;
    }

    public void setRequest(Search request)
    {
        this.request = request;
    }
}
