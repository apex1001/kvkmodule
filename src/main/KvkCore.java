package main;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.clients.RESTClient;
import main.clients.StompListener;
import main.clients.StompSender;
import main.entity.SearchRequest;

/*
 * Main KvK Module
 * 
 * Waits for search request messages in the given request queue. 
 * Result is posted to the given response queue
 * 
 * @author V. Vogelesang
 */
public class KvkCore
{
    private final String serviceUrl;
    private StompListener listener;
    private StompSender sender;
    private RESTClient restClient;
    // Logger object
    private static final Logger LOGGER = Logger.getLogger(StompListener.class.getName());

    /**
     * KvkCore constructor
     *
     * @param listener   The StompListener, listening for search requests.
     * @param sender     The StompSender, used for sending messages.
     * @param restClient The REST client to perform REST requests.
     * @param serviceUrl The URL of the REST service.
     */
    public KvkCore(StompListener listener, StompSender sender, RESTClient restClient, String serviceUrl)
    {
        this.listener = listener;
        this.sender = sender;
        this.restClient = restClient;
        this.serviceUrl = serviceUrl;
    }

    /**
     * Start the search process
     */
    public void start()
    {
        //noinspection InfiniteLoopStatement
        while (true)
        {
            // Wait for company search message from facade
            try
            {
                listenForMessage();
            }
            catch (Exception e)
            {
                LOGGER.log(Level.SEVERE, "Exception caught: " + e.getMessage(), e);
            }
        }
    }

    /**
     * Listen for a message from the queue and retrieve company information
     * via the REST client.
     *
     * @throws IOException
     */
    public void listenForMessage() throws IOException
    {
        final SearchRequest searchRequest = listener.getMessage();
        String company = searchRequest.getRequest().getString();

        if (company != null && company.length() > 0)
            processSearchRequest(searchRequest);
    }

    /**
     * Process the received search request.
     *
     * @param searchRequest The search request to process.
     * @throws IOException Thrown by the sender.
     */
    private void processSearchRequest(SearchRequest searchRequest) throws IOException
    {
        // Get results from company name
        String company = searchRequest.getRequest().getString();
        String result = restClient.getResult(serviceUrl, company);

        if (result != null && result.length() > 0)
            // Send message into queue for facade.
            sender.sendMessage(result, searchRequest.getId());
    }
}
