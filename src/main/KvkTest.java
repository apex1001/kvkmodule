package main;

import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;

import main.clients.RESTClient;

import org.junit.Test;

/**
 * Test KvkModule unit
 * 
 * @author V. Vogelesang
 * 
 */
public class KvkTest {

	@Test
	public void test() throws UnsupportedEncodingException {

		// Test REST clients
		testRestClient();
	}

	/**
	 * Test REST Client
	 * 
	 * @throws UnsupportedEncodingException
	 */
	private void testRestClient() throws UnsupportedEncodingException {

		// Arrange
		RESTClient rest = new RESTClient();

		// Act
		String result1 = rest.getResult("http://officieel.openkvk.nl/xml/",
				"koninklijke philips");
		String result2 = rest.getResult("http://officieel.openkvk.nl/xml/",
				"purr");

		String ex = null;
		try {
			String result3 = rest.getResult("http://officieel.openkvk.nl/xml/",
					"dddddddddddddd");
		} catch (Exception e) {
			ex = e.getMessage();
		}

		// Check for valid xml string and some company details
		assertTrue("", result1.length() > 0);
		assertTrue("", result1.contains("<rechtspersonen>"));
		assertTrue("", result1.contains("<rechtspersoon id=\""));
		assertTrue("", result1.contains("</rechtspersonen>"));
		assertTrue("", result1.contains("Koninklijke Philips N.V."));
		assertTrue("", result1.contains("170019100001"));

		// Check for valid xml string and some company details
		assertTrue("", result2.length() > 0);
		assertTrue("", result2.contains("<rechtspersonen>"));
		assertTrue("", result2.contains("<rechtspersoon id=\""));
		assertTrue("", result2.contains("</rechtspersonen>"));
		assertTrue("", result2.contains("Gebr. Purr B.V."));
		assertTrue("", result2.contains("080729250000"));

		// Check for 404 error
		assertTrue("", ex.contains("404"));
	}
}
