package main.clients;

import main.entity.Search;
import main.entity.SearchRequest;
import main.stomp.ConnectionFactory;
import org.apache.activemq.transport.stomp.StompConnection;
import org.apache.activemq.transport.stomp.StompFrame;

import javax.xml.bind.JAXB;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Logger;

/*
 * Listens for a Stomp message in the given queue
 * 
 * @author V. Vogelesang
 */
public class StompListener
{
    private static final long TIMEOUT = 300000L;
    private final ConnectionFactory connectionFactory;
    private final String queue;
    // Logger object
    private static final Logger LOGGER = Logger.getLogger(StompListener.class.getName());

    /**
     * Create new stomp listener
     *
     * @param connectionFactory The STOMP connection factory.
     * @param queue The queue name to subscribe to.
     */
    public StompListener(ConnectionFactory connectionFactory, String queue)
    {
        this.connectionFactory = connectionFactory;
        this.queue = queue;
    }

    /**
     * Returns STOMP message content
     *
     * @return The search request.
     */
    public SearchRequest getMessage() throws IOException
    {
        StompConnection connection = null;

        try
        {
            // Create connection & transaction
            connection = getConnection();
            LOGGER.info("Waiting for messages..");
            final StompFrame message = waitForMessage(connection);

            // Get search string from XML
            LOGGER.info("Message received: " + message);
            return getSearchRequestFromMessage(message);
        }
        finally
        {
            // Close connection
            if (connection != null)
                connection.close();
        }
    }

    /**
     * Wait for a message to be delivered.
     *
     * @param connection The STOMP connection to listen with.
     * @return The received message.
     * @throws IOException Thrown if an error occurs.
     */
    private StompFrame waitForMessage(StompConnection connection) throws IOException
    {
        try
        {
            // Wait for message, this is blocking.
            StompFrame message = connection.receive(TIMEOUT);
            String messageId = message.getHeaders().get("message-id");
            if (messageId != null)
                connection.ack(messageId);
            else
                LOGGER.severe("No message id found in received message");
            return message;
        }
        catch (Exception e)
        {
            // Convert the exception to an IO Exception.
            throw new IOException(e);
        }
    }

    /**
     * Get a new connection.
     *
     * @return The newly created connection.
     * @throws IOException
     */
    private StompConnection getConnection() throws IOException
    {
        try
        {
            StompConnection connection = connectionFactory.getConnection();
            connection.subscribe(queue);
            return connection;
        }
        catch (Exception e)
        {
            // Convert the generic exception to an IOException.
            throw new IOException(e);
        }
    }

    /**
     * Get the search request.
     *
     * @param message The received STOMP message
     * @return The search request data.
     */
    private SearchRequest getSearchRequestFromMessage(StompFrame message)
    {
        final String xmlString = message.getBody();
        // Create a new request object.
        SearchRequest request = new SearchRequest();
        // The xmlString supposedly contains the Search data.
        ByteArrayInputStream is = new ByteArrayInputStream(xmlString.getBytes());
        request.setRequest(JAXB.unmarshal(is, Search.class));
        request.setId(message.getHeaders().get("correlation-id"));
        return request;
    }
}
