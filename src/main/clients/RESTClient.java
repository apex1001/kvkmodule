package main.clients;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * Gets company info from given KvK url
 * 
 * @author V. Vogelesang
 */
public class RESTClient
{
    /**
     * Gets company information from a REST URL.
     *
     * @param url The URL to get the "result" from.
     * @param company The company to look up.
     * @return Result from REST resource.
     */
    public String getResult(String url, String company) throws UnsupportedEncodingException
    {
        DefaultClientConfig clientConfig = new DefaultClientConfig();
        Client client = Client.create(clientConfig);
        WebResource resource = client.resource(url + URLEncoder.encode(company, "UTF-8"));

        // Get result
        return resource.accept(MediaType.APPLICATION_XML).get(String.class);
    }
}
