package main.clients;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import main.stomp.ConnectionFactory;
import org.apache.activemq.transport.stomp.StompConnection;

import javax.ws.rs.core.MediaType;

/*
 * Sends a STOMP message into the given queue
 * 
 * @author V. Vogelesang
 */
public class StompSender
{
    private final String queue;
    private final ConnectionFactory connectionFactory;
    // Logger object
    private static final  Logger LOGGER = Logger.getLogger(StompSender.class.getName());

    /**
     * Create new stomp sender
     *
     * @param connectionFactory The STOMP connection factory.
     * @param queue    The queue to send to.
     */
    public StompSender(ConnectionFactory connectionFactory, String queue)
    {
        this.queue = queue;
        this.connectionFactory = connectionFactory;
    }

    /**
     * Send the STOMP message into the queue
     *
     * @param content The XML content to send.
     */
    public void sendMessage(String content, String correlationId) throws IOException
    {
        StompConnection connection = null;
        final String transactionName = "tx";
        try
        {
            // Create connection
            connection = connectionFactory.getConnection();

            // Send message
            connection.begin(transactionName);
            connection.send(queue, content, transactionName, getMessageHeaders(correlationId));
            connection.commit(transactionName);

            LOGGER.info("Successfully sent message to " + queue
                    + "\nPayload: \n" + content + "..");

        }
        catch (Exception e)
        {
            throw new IOException(e);
        }
        finally
        {
            closeConnection(connection);
        }
    }

    private void closeConnection(StompConnection connection) throws IOException
    {
        // Close connection if available
        if (connection != null)
        {
            try
            {
                connection.disconnect();
                connection.close();
            }
            catch (Exception e)
            {
                throw new IOException(e);
            }
        }
    }


    /**
     * Generate a map of message headers.
     *
     * @param correlationId The correlation id to add to the message.
     * @return The map of message headers.
     */
    public HashMap<String, String> getMessageHeaders(String correlationId)
    {
        HashMap<String, String> headers = new HashMap<>();
        // This is implicit, as all communication is xml.
        headers.put("Content-Type", MediaType.APPLICATION_XML);
        // The correlation id is required, as the remote side will otherwise not know
        // to which the reply message is related.
        headers.put("correlation-id", correlationId);
        return headers;
    }
}
