package main.stomp;

import org.apache.activemq.transport.stomp.StompConnection;

import java.io.IOException;
import java.util.HashMap;

/**
 * Connection factory for STOMP connections.
 */
public class ConnectionFactory
{
    private final String hostname;
    private final int port;
    private final String user;
    private final String pass;
    private final String virtualHost;

    /**
     * ConnectionFactory constructor.
     *
     * @param hostname The hostname to connect to
     * @param port     The port to connect to
     * @param user     The username to connect with
     * @param pass     The password to connect with
     */
    public ConnectionFactory(String hostname, int port, String user, String pass)
    {
        this(hostname, port, user, pass, null);
    }

    /**
     * Create new stomp listener
     *
     * @param hostname The hostname to connect to
     * @param port     The port to connect to
     * @param user     The username to connect with
     * @param pass     The password to connect with
     * @param virtualHost The virtual host to connect to (in case of rabbitmq).
     */
    public ConnectionFactory(String hostname, int port, String user, String pass,
                         String virtualHost)
    {
        this.hostname = hostname;
        this.port = port;
        this.user = user;
        this.pass = pass;
        this.virtualHost = virtualHost;
    }


    /**
     * Get a new connection.
     *
     * @return The newly created connection.
     * @throws java.io.IOException
     */
    public StompConnection getConnection() throws IOException
    {
        try
        {
            StompConnection connection = new StompConnection();
            connection.open(hostname, port);
            connection.connect(getConnectionHeaders());
            return connection;
        }
        catch (Exception e)
        {
            // Convert the generic exception to an IOException.
            // The ActiveMQ STOMP library throws generic exceptions which sucks.
            // It is likely that the exception thrown by the ActiveMQ library is an
            // IOException.
            throw new IOException(e);
        }
    }

    /**
     * Get the headers for the CONNECT command.
     * @return The HashMap of headers.
     */
    private HashMap<String, String> getConnectionHeaders()
    {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("login", user);
        headers.put("passcode", pass);
        // Add the host header if the virtual host is set in the config.
        if (virtualHost != null)
            headers.put("host", virtualHost);
        return headers;
    }

}
